'''Saves the first N lines of a file named "filename" in a list'''
def word_to_list (filename, N):
	fn = open(filename)
	alist = []
	for i in range(N):
		alist.append(next(fn).strip())
	fn.close()
	return alist

'''Saves every line of a file named "filename" in a list'''
def all_word_to_list (filename):
	fn = open(filename)
	alist = []
	for line in fn:
		word = line.strip()
		alist.append(word)
	fn.close()
	return alist

def main():
	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	print '          Testing "word_to_list" function'
	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	# Ask user to give the number of lines
	N = int(input('Give the number of lines:'))

	print '-----------------------------------------------------------'
	print 'First', N, ' words of the file:'
	print word_to_list('words.txt',N)

	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	print '          Testing "all_word_to_list" function'
	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'

	#Since "words.txt" is very big, we give the opportunity to the reader to print the whole file
	switch = raw_input('Do you want to print all the words? (yes/no)')

	#we print by deafault only the number of words
	print '-----------------------------------------------------------'
	print 'Number of words in the words.txt:',
	x = []
	x = all_word_to_list('words.txt')
	print len(x)

	if switch == 'yes':
		print x

if __name__ == '__main__':
	main()



