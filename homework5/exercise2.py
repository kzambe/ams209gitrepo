'''Takes a file and gives a String'''
def file_to_string(filename):
	filestring = open(filename).read()
	return filestring

'''Counts the number of occurences of "acharacter" in "astring"'''
def occurences_counter(astring,acharacter):
	return astring.count(acharacter)

def occurences_in_file(filename):
	
	astring = file_to_string(filename)
	occur_table = {}

	for character in astring:
		if character not in occur_table:
			occur_table[character] = 1
		else:
			occur_table[character] = occur_table[character] + 1

	return occur_table

'''Prints a dict in a tuple way'''
def print_dict(adict):
	for key in adict:
		print (key, adict[key]) 


def main():

	print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	print '  Testing "occurences_in_file" function in the file "words.txt"'
	print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'

	print_dict(occurences_in_file('words.txt'))
if __name__ == '__main__':
	main()