'''The dict meter_ratios keeps the constants of conversion from meter to everything else'''
meter_ratios = {}
meter_ratios['yard'] = 1.093613
meter_ratios['mile'] = 0.000621
meter_ratios['foot'] = 3.28084
meter_ratios['inch'] = 39.370079
meter_ratios['meter'] = 1.
meter_ratios['km'] = 10**(-3)
meter_ratios['cm'] = 10**2
meter_ratios['mm'] = 10**3
meter_ratios['nm'] = 10**9
meter_ratios['um'] = 10**6

#This dictionary will keep the outputs
outputs = {'yard': -1, 'mile': -1, 'meter':-1 , 'foot':-1, 'inch':-1}

def user_input():

	# Ask user for unit value
	print '            Length converter is starting...'
	print '---------------------------------------------------------------------------'
	user_unit = raw_input('Please type a unit system (meter, mile, inch, foot, yard): ')
	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'

	#Check if the input is valid
	if user_unit not in meter_ratios.keys():
		print 'This is not a valid unit!'
		print 'Program will terminate...'
		exit()

	# Ask user for length value
	user_str_value = raw_input('Please input a length (numbers only): ')


	#Check if the input is valid
	try:
		float(user_str_value)
	except ValueError:
		print 'This is not a valid value!'
		print 'Program will terminate...'
		exit()

	user_value = float(user_str_value)
	return user_unit, user_value

def main():
	#Run user_input and keep units in "unit" and length to "length"
	unit, length = user_input()

	for element in meter_ratios.keys():
		if (element != unit):
			outputs[element] = (1./meter_ratios[unit])*length*meter_ratios[element]
	display_output(unit, length)

def display_output(unit, length):
	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	print '      ', length, unit, 'are equal to:'
	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	for element in meter_ratios.keys():
		if (element != unit):
			print '------->>>>>>>', outputs[element], element

if __name__ == '__main__':
	main()