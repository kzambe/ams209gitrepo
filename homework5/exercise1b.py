'''Saves the first N lines of a file named "filename" in a dict as keys with values "null" '''
def word_to_dict (afile, N):
	fn = open(afile)
	adict = {}
	for i in range(N):
		adict[next(fn).strip()] = "null"
	fn.close()
	return adict

'''Saves every line of a file named "filename" in a dict as keys with values "null" '''
def all_word_to_dict (afile):
	fn = open(afile)
	adict = {}
	for line in fn:
		word = line.strip()
		adict[word] = "null"
	fn.close()
	return adict

def main():
	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	print '          Testing "word_to_dict" function'
	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	# Ask user to give the number of lines
	N = int(input('Give the number of lines:'))

	print '-----------------------------------------------------------'
	print 'First', N, ' words of the file:'
	print word_to_dict('words.txt',N)

	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	print '          Testing "all_word_to_dict" function'
	print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'

	#Since "words.txt" is very big, we give the opportunity to the reader to print the whole file
	switch = raw_input('Do you want to print all the words? (yes/no)')

	#we print by deafault only the number of words
	print '-----------------------------------------------------------'
	print 'Number of words in the words.txt:',
	x = []
	x = all_word_to_dict('words.txt')
	print len(x)

	if switch == 'yes':
		print x

if __name__ == '__main__':
	main()
