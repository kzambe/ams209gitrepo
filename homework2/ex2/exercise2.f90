!----------------------------------------------------------------
! Run this program in bash using the command on 'HowToRunMe.txt'
!----------------------------------------------------------------

program PiApproximation

 implicit none
  
  real, parameter :: threshold = 1.e-12        ! The approximation will be at least that close to real
  real, parameter :: pi_true = acos(-1.d0)     ! Real pi
 
  real :: pi_approx = 0.0                      ! Our approximation of pi
  real :: diff = 1.0                           ! Error 
 
  integer :: n = 0

  do while (diff>threshold)
 
    pi_approx = pi_approx +( (4.0/(8.0*n+1.0)-2.0/(8.0*n+4.0)-1/(8.0*n+5.0)-1.0/(8.0*n+6.0))*16.0**(-n))
   
    n = n + 1

    diff = abs(pi_approx - pi_true)

 end do

 write(*,*) 'Pi Approximation = ', pi_approx,  'Real Pi =', pi_true
 write(*,*) 'Number of iterations = ', n, 'Error = ', diff

end program PiApproximation

