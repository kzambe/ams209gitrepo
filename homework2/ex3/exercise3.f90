!*******************************************************************************
! To run the program in bash use the command on 'HowToRunMe.txt'
!*******************************************************************************


!---------------------------------------------------------------------------------
! Define the real function to be inegrated
!---------------------------------------------------------------------------------

real (kind=8) function f(x)

 implicit none
  real (kind=8), intent(in) :: x

 f = x**2 + 1

end function f

!----------------------------------------------------------------------------------
! An integrant of the above function f
!----------------------------------------------------------------------------------

real (kind=8) function IntF(x)

 implicit none
  real (kind=8), intent(in) :: x

 Intf = (x**3)/3 + x

end function IntF




!----------------------------------------------------------------------------------  
! The  trapezoidApprox module 
!
!*********************************************************************************
! Integrates the function f defined above using trapezoid method
! It includes two implementations,  one in function and another in subroutine
!----------------------------------------------------------------------------------

module trapezoidApprox

 contains
 
 !--------------------------
 ! Function implementation
 !--------------------------


 real (kind=8) function trapezoidFunct(N, start, finish)
 
  implicit none 

  integer, intent(in) :: N                    ! size of your partition 
  real (kind=8), intent(in) :: start          ! staring endpoint of the interval
  real (kind=8), intent(in) :: finish         ! ending endpoint of the  interval
 
  real (kind=8), external :: f                ! the function f we want to integrate

  integer :: i = 0 
  real (kind=8) :: integral = 0

  do while(i<N)
 
   integral = integral +(( f(start + (finish-start)*i*(1.0/N)) + &
         f(start + (finish-start)*(i+1.0)*(1.0/N)) )/2.0)*1/N

   i = i + 1

  end do
 
  trapezoidFunct = integral

 end function trapezoidFunct

 !---------------------------
 ! Subroutine implementation
 !---------------------------

 subroutine trapezoidSubr(N, start, finish, integral)
  
  implicit none 

  integer, intent(in) :: N                     ! size of your partition 
  real (kind=8), intent(in) :: start           ! staring endpoint of the interval
  real (kind=8), intent(in) :: finish          ! ending endpoint of the  interval
  real (kind=8), intent(out) :: integral
  
  real (kind=8), external :: f                 ! the function f we want to integrate

  
  integer :: i = 0 
 
  do while(i<N)
 
   integral = integral +(( f(start + (finish-start)*i*(1.0/N)) + &
         f(start + (finish-start)*(i+1.0)*(1.0/N)) )/2.0)*1/N

   i = i + 1

  end do

 end subroutine trapezoidSubr

 !------------------------
 ! Exact Integration
 !------------------------

 subroutine ExactIntegral(start, finish, integral)

  implicit none

  real (kind=8), intent(in) :: start
  real (kind=8), intent(in) :: finish
  real (kind=8), intent(out) :: integral

  real (kind=8), external :: IntF

  integral = IntF( finish ) - IntF( start )


 end subroutine ExactIntegral

end module trapezoidApprox

!--------------------------------------------------------------------------------
! Diver Routine
!--------------------------------------------------------------------------------

program ComputeIntegral

 use trapezoidApprox

 implicit none
 
  integer :: N = 400
  real (kind=8) :: a = 0
  real (kind=8) :: b = 1
 
  real (kind=8) :: ApproxIntFunct
  real (kind=8) :: ApproxIntSubr
  real (kind=8) :: ExactInt

  ApproxIntFunct = trapezoidFunct(N, a, b)
  
  call trapezoidSubr(N, a, b, ApproxIntSubr)
  
  call ExactIntegral(a, b, ExactInt)


    print*, "[1] Trapezoidal in function   =", ApproxIntFunct
    print*, "[2] Trapezoidal in subroutine =", ApproxIntSubr
    print*, "[3] Exact integration         =", ExactInt
    print*, "[4] Error in function         =", abs(ExactInt - ApproxIntFunct)
    print*, "[5] Error in subroutine       =", abs(ExactInt - ApproxIntSubr)



end program ComputeIntegral


