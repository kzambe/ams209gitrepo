"""The function below takes a list containing the last and the
    first name of a person and prints and a boolean. It prints the
    full name with the first letters capitalized in one
    of two formats selected by the boolean"""

def print_yourName(alist, print_type = True):
		
	alist[0] = alist[0][:1].upper() + alist[0][1:]
	alist[1] = alist[1][:1].upper() + alist[1][1:]
	
	if (print_type == True):
		
		print alist[0] + " " + alist[1]
	
	else :

		print alist[1] + ", " + alist[0]


#Testing
def main():
	print_yourName(['zampetakis', 'kostas'], False)
	print_yourName(['zampetakis', 'kostas'], True)
	print_yourName(['zampetakis', 'kostas'])

if __name__ == '__main__':
	main()