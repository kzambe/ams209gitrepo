"""This function takes three real numbers; it outputs True if they
    correspond to lenghts of line segments that can form a
    triangle and False otherwise"""

def can_form_tringle(a,b,c):
	if (abs(b-c) <= a) and (a <= b+c):
		return True
	else :
		return False

#Testing

def main():
	print 'Test 1', can_form_tringle(8,15,4)
	print 'Test 2', can_form_tringle(5,3,4)
	print 'Test 3', can_form_tringle(7,15,6)
	print 'Test 4', can_form_tringle(15,7,6)
	print 'Test 5', can_form_tringle(15,6,7)
	print 'Test 6', can_form_tringle(6,7,15)


if __name__ == '__main__':
	main()