"""This function takes an array and returns True if the array is
    sorted and False otherwise"""

def is_sorted(alist):
	for i in range(len(alist)-1):
		if alist[i] > alist[i+1]:
			return False
	return True



#Testing
def main():
	print 'Test 1', is_sorted([2,2,3])
	print 'Test 2', is_sorted(['b', 'a'])


if __name__ == '__main__':
	main()
