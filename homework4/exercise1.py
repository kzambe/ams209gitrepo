"""The function below takes a string and prints it with
    enough leading spaces so that the last letter
    of the string is in column 70 of the display  """


def right_justify (String):
	for i in range (70 - len(String)):
		print "",
	print String


#-----------------------------------------------------
# To run it with the string of your choice, type your
# own string in the place of the string "geia"
#-----------------------------------------------------

def main():
	right_justify("geia")

if __name__ == '__main__':
	main()
