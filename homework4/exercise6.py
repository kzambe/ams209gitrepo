"""The function below takes a String and returns True if the
    String is palindrome and False otherwise"""

def is_palindrom(String):
	for i in range((len(String) +1)/2):
		if (String[i] != String[-i-1]):
		 	return False
	return True


#Testing

def main():
	print 'Test 1', is_palindrom('dadwetewdad')
	print 'Test 2', is_palindrom('dadweewdad')
	print 'Test 3', is_palindrom('dadweeydad')


if __name__ == '__main__':
	main()