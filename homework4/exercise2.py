"""The first function takes a function and a value and calls the function twice with this value as an argument"""

def do_twice(f, value):
      f(value)
      f(value)


def print_twice(String):
      print String
      print String

#Testing
#prints 4 times the argument 'spam'
def main():
	do_twice(print_twice, 'spam')

if __name__ == '__main__':
	main()