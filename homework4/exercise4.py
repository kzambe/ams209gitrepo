"""This function takes a list and returns the list of partial
    sums"""

def sum_list(alist):
	for i in range (1, len(alist)):
		alist[i] = alist[i] + alist[i-1]
	return alist


#testing

def main():
	print 'Test 1', sum_list([1,2,3,4])
	print 'Test 2', sum_list([])
	print 'Test 3', sum_list([1,-1,4])

if __name__ == '__main__':
	main()