\documentclass{article}


\usepackage{amsmath, enumerate}
\usepackage{fullpage}
\usepackage{verbatim}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage[]{algorithm2e}

\usepackage{pstricks}
\usepackage{pst-plot}
\usepackage{pstricks-add}
\usepackage{epsf}

\usepackage{float}

\usepackage{hyperref}

\usepackage{epstopdf}
\usepackage[section]{placeins}
%\usepackage{auto-pst-pdf}
%\DeclareGraphicsExtensions{.ps}
%\epstopdfDeclareGraphicsRule{.ps}{pdf}{.pdf}{ps2pdf -dEPSCrop -dNOSAFER #1 \OutputFile}


\title{AMS209 Final Project:\\[0.1cm]
\large Numerical Linear Algebra using Fortran and Python}

\begin{document}
\maketitle

\begin{abstract}
	In this report I present my work for the final project of the course \emph{AMS209 Scientific Comptuting} on the Fall quarter of 2017. 
	In this project we implemented the \emph{gaussian elimination} algorithm to obtain an $LU$-decomposition of a square matrix. 
	We implemented two versions of this algorithm; one follows the usual algorithm, without any further check on the pivot elements while 
	the second implementation uses \emph{partial pivoting}, i.e., in each iteration sets the greatest  element in absolute value of the remaining 
	rows as pivot element. We then use these decompositions to solve linear systems. Both algorithms are implemented in 
	Fortran$90$ language, while a Python script handles the execution of our main
	code and produces plots of our results. 
\end{abstract}


\section*{Introduction}
The $LU$-decomposition is a factorization of square matrix matrix $A$ 

\begin{align}
	A = LU
	\label{one}
\end{align}
into a product of two special matrices, a lower triangular $L$ and an upper triangular $U$. Given an $LU$-demposition of $A$ many interesting problems, such as finding the solution of a linear system $Ax = b$ or  
computing 
$\mathrm{det}(A)$, can be easily addressed by basically ``reading" $L$ and $U$. 

Not all matrices admit an $LU$-decomposition, thought. However, 
if we relax our factorization further to what is called an \emph{$LUP$-decomposition}, then the factorization can be achieved 
on any square matrix.  An $LUP$-decomposition is an $LU$-decompotion of the square matrix $PA$ 

\begin{align}
	PA=LU
	\label{two}
\end{align}

where $P$ is a permutation matrix
\footnote{A square matrix $P$ is a \emph{permutaion matrix} if $P$ can be produced by a series of row interchanges of the identity matrix $I$ with the same dimension.}. Since multiplying $A$ with $P$, has as only effect interchanging the rows of $A$, this factorization remains useful. 

While such an $LU$-decomposition may seem difficult to produced, it can be computed efficiently using the landmark method of gaussian elimination. To produce an $LUP$-decomposition, one can modify slightly the gaussian elimination doing an additional step of row interchange in each iteration, known as \emph{pivoting}.

\section*{Methods}

	As we mentioned above, to obtain an $LU$-decomposition of an $n \times n$ matrix $A$, we use the gaussian elimination (GE) algorithm. 
	While running GE, we keep 
	track of the row multipliers on the matrix $L$, initialized as identity matrix, and we update the values of $U$, which is initialized as $A$,
	 in order to maintain the factorization \eqref{one}. We can 
	summarize the method in the following pseudocode
\bigskip	

\begin{algorithm}[H]

 Initialize $A = U$ and $L = I$\;
 \For{$i = 1, 2, \dots, n-1$ }{
 	\For{$j = k+1, k+2, \dots, n$ }{
$L(j,i)  = U(j,i)/U(i,i)$\;
 $U(j,i:n) = U(j, i:n) - L(j,i)U(i,i:n)$\;}
 }
 \caption{$LU$-decomposition}
\end{algorithm}

The above algorithm will crush if we encounter a zero in the diagonal of $U$, as we will need to perform a division by zero. Moreover, even if no 
diagonal element of $U$ is zero, but small enough, the algorithm becomes numerically unstable. 

To overcome these issues, before proceeding to the $j$-th step in the algorithm above, we interchange the current row, $i$, of $U$ with the	
row containing the maximum absolute value element of the $j$-th column of the rows below. However, we need to keep track 
of those row swaps as they affect both $L$ and $U$. The initial matrix, $A$, can be retrieved using matrix $P$ which contains the whole swap history, resulting to an $LUP$-decomposition of $A$
%say about the changes in both L and U
\bigskip

\begin{algorithm}[H]

 Initialize $A = U$, $L = I$ and $P = I$\;
 \For{$i = 1, 2, \dots, n-1$ }{
 	find index $k \geq i$ maximizing $|U(k,i)|$\;
	\textsc{Swap} $\{U(i,i:n), U(k,i:n)\}$\;
	\textsc{Swap} $\{L(i,1:i-1), L(k,1:i-1)\}$\;
	\textsc{Swap} $\{P(i,:), P(k,:)\}$\;
 	\For{$j = k+1, k+2, \dots, n$ }{
$L(j,i)  = U(j,i)/U(i,i)$\;
 $U(j,i:n) = U(j, i:n) - L(j,i)U(i,i:n)$\;}
 }
 \caption{$LUP$-decomposition}
\end{algorithm}

Given an $LU$ or $LUP$-decomposition of a square matrix $A$, it is easy to solve a linear  system of the form $Ax=b$, since 
we can break it in to two simpler systems

\begin{enumerate}
\item Ly = b
\item Ux = y
\end{enumerate}

both of which can be solved quickly due to the triangular structure of $L$ and $U$. The idea is to solve the first system row by row, starting 
from the first row
and substituting the solutions found to the next row; this method is called \emph{forward substitution}.  Given the solution $y$ of the first system,
the second can be solved system can be solved in the same way, starting this time from the bottom row; this is known as \emph{forward substitution}.

\section*{Implementation}
In the implementation we are given a matrix $A$ and a vector $b$, and we are asked to first decompose $A$ finding both $LU$ and $LUP$ factorization of $A$, and then use $L$ an $U$ to solve $Ax = b$.
For the whole implementation we used Fortran90. The code is organized in modular way under the directory \texttt{LinAlg}, with \texttt{linear$\_$solve.f90} serving as the driver routine, as suggested by Prof. Dongwook. To run the program, you simply need to move to the directory
\texttt{PyRun} and run the python script \texttt{pyRun$\_$LU.py}.
	
	
\section*{Results}



Our algorithm is tested in three different cases 

\begin{enumerate}

\item \[A = \begin{pmatrix*}
\phantom{-}1  &\phantom{-}1 &-1\\
\phantom{-}1 &\phantom{-}2 &-2\\
-2 &\phantom{-}1 &\phantom{-}1
\end{pmatrix*}, b =  \begin{bmatrix}1\\
0\\
1 \end{bmatrix}\]\\

\item \[A = \begin{pmatrix*}
4 &3 & 2&1 \\
3 & 4 & 3 & 2\\
2 &3 &4 &3\\
1 &2 &3 &4
\end{pmatrix*}, b = \begin{bmatrix}\phantom{-}1 \\
\phantom{-}1\\
-1\\
-1
\end{bmatrix}\]\\

\item \[A = \begin{pmatrix*}
\phantom{-}1 &-1 &\phantom{-}1  &-1\\
-1 &\phantom{-}3 &-3  &\phantom{-}3\\
\phantom{-}2 &-4 &\phantom{-}7  &-7 \\
-3 &\phantom{-}7 &-10 &\phantom{-}14
\end{pmatrix*}, b= \begin{bmatrix}
\phantom{-}0\\
\phantom{-}2\\
-2\\
-8
\end{bmatrix}\]
\end{enumerate}

\bigskip

For each of them we print on screen the matrix $A$ and the vector $b$ for a sanity check, then we compute the $LU$ and $LUP$ decomposition and the solution $x$ via the two different methods,
print them also on screen and we save the solutions $x$  on the \texttt{LinAlg} folder. Finally, using \texttt{matplotlib} packahe we generate the following graphs for every case
\newpage

	\begin{figure}[!htb]
		\centering
		\includegraphics[scale=0.7]{../LinAlg/A_2.png}
		\caption{Plot of matrix $A$ for the first test case}
	\end{figure}
	\begin{figure}[!htb]
		\centering
		\includegraphics[scale=0.8]{../LinAlg/xb_2.png}
		\caption{Plot of solution and $b$ vectors for the first test case}
	\end{figure}	
	
		\begin{figure}[!htb]
		\centering
		\includegraphics[scale=0.8]{../LinAlg/A_3.png}
		\caption{Plot of matrix $A$ for the second test case}
	\end{figure}


	\begin{figure}[!htb]
		\centering
		\includegraphics[scale=0.8]{../LinAlg/xb_3.png}
		\caption{Plot of solution and $b$ vectors for the second test case}
	\end{figure}	
	
			\begin{figure}[!htb]
		\centering
		\includegraphics[scale=0.8]{../LinAlg/A_1.png}
		\caption{Plot of matrix $A$ for the third test case}
	\end{figure}


	\begin{figure}[!htb]
		\centering
		\includegraphics[scale=0.8]{../LinAlg/xb_1.png}
		\caption{Plot of solution and $b$ vectors for the third test case}
	\end{figure}	
	
	
	
	

	\newpage
	
\section*{Comments-Conclusion}
	
	Although, in the particular test-cases the results produced by both methods were almost identical, $LUP$-decomposition will always work
	and give greater accuracy.  
	
	Implementing this project in modular programming with Fortran was not an easy task, by any 
	means, especially because of the difficult debugging process. However, in Fortran90 features as slicing notation and many built-in 
	functions make the code handy and easy to transfer from pseudocode. 
	 
	\end{document}


 