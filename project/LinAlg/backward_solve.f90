module backward_solve
    
    implicit none

    contains

subroutine find_x(dim, U, x, b)
    
    implicit none
    
    integer, intent(in) :: dim
    real(kind=8), dimension(dim,dim), intent(in) :: U
    real(kind=8), dimension(dim), intent(in) :: b
    real(kind=8), dimension(dim), intent(out) :: x

    integer :: i



    x(dim) = b(dim)/U(dim,dim)

    if (dim == 1) then
        
        stop
    endif

    do i = 1, dim-1
        x(dim-i) = (-dot_product(U(dim-i,dim-i+1:), x(dim-i+1:))+ b(dim-i))/U(dim-i,dim-i)
    end do

end subroutine find_x



end module backward_solve