!Human-friendly printing to screen
module write_to_screen

contains

  !Printing input matrices A and b to screen for sanity check
  subroutine print_inputs(A,b,n)
    implicit none
    
    integer, intent(in) :: n
    real(kind=8), dimension(n), intent(in) :: b
    real(kind=8), dimension(n,n), intent(in) :: A

    print*,'++++++++++++++++++++++++++++++++++++++++++++++++'
    print*,'               Sanity Check                     '
    print*,'++++++++++++++++++++++++++++++++++++++++++++++++'
    print*,''
    print*,"A = "
    call print_matrix(A,n)
    print*,''
    print*,'-------------------------------------------------'
    print*,''
    print*,"b = "
    call print_vector(b,n)
    print*,'++++++++++++++++++++++++++++++++++++++++++++++++'
    print*,''

  end subroutine

  !!Print a square matrix to screen in Human-friendly way
  subroutine print_matrix(A,n)
    implicit none
    integer, intent(in) :: n
    real(kind=8), dimension(n,n), intent(in) :: A

    integer :: i,j
    
    do, i=1, n
    write(*,"(100g15.5)") ( A(i,j), j=1, n )
    end do

  end subroutine

  !!Prints vector to screen in nicely manner
  subroutine print_vector(b,n)
    implicit none
    integer, intent(in) :: n
    real(kind=8), dimension(n), intent(in) :: b

    integer :: i
    do i=1,n
      print *,b(i)
    end do

  end subroutine print_vector
 

end module write_to_screen