module forward_solve
    
    implicit none

    contains

subroutine find_y(dim, L, y, b)
    
    implicit none
    
    integer, intent(in) :: dim
    real(kind=8), dimension(dim,dim), intent(in) :: L
    real(kind=8), dimension(dim), intent(in) :: b
    real(kind=8), dimension(dim), intent(out) :: y

    integer :: i



    y(1) = b(1)

    if (dim == 1) then
        
        stop
    endif

    do i = 2, dim
        y(i) = -dot_product(L(i,:i-1), y(:i-1))+ b(i)
    end do

end subroutine find_y



end module forward_solve