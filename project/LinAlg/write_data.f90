!Writes result to screen as well as to a file
module write_data

  use write_to_screen,  only : print_vector
  !use read_data

contains

  !Print solution to screen and file x.dat
  subroutine write_solution(afilename,x,n)
    implicit none
    integer, intent(IN) :: n
    integer :: i

    real(kind=8), dimension(n), intent(IN) :: x
    character(len=:), allocatable, intent(IN) :: afilename
    
    !Print solution to screen and onto file
    print*,'++++++++++++++++++++++++++++++++++++++++++++++++'
    print*,'                 Result  '
    print*,'++++++++++++++++++++++++++++++++++++++++++++++++'
    print*,"x = "
    call print_vector(x,n)

    open(13,file = afilename)
    do i=1,n
      write(13,*) x(i)
    end do

  end subroutine write_solution

end module write_data