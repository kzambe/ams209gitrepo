module LU_decomp
    implicit none

    contains

!Creates a indentity matrix of size "dim"
subroutine ident(dim, I)

    implicit none 

    integer, intent(in) :: dim
    real(kind=8), dimension(dim, dim), intent(out) :: I

    integer :: inda, indb 

    do inda=1,dim
        
        do indb=1,dim

            if (inda == indb) then 

                I(inda, indb) = 1

            else

                I(inda, indb) = 0

            end if

        enddo
    
    enddo 

end subroutine ident



!Inercanges elements in lines "aline" and "bline" from "acol" to "bcol"
!of a "dim" X "dim" matrix amatrix
subroutine swap(dim, amatrix, aline, bline, acol, bcol)
    
    implicit none
    

    integer, intent(in) :: acol, bcol, aline, bline, dim
    real(kind=8), dimension(dim,dim), intent(inout) :: amatrix
    
    real(kind=8), dimension(dim) :: temp_vec

    temp_vec = amatrix(aline,:) 
    amatrix(aline, acol:bcol) = amatrix(bline, acol:bcol)
    amatrix(bline, acol:bcol) = temp_vec(acol:bcol)

end subroutine swap


subroutine vec_swap(dim, vec, aline, bline)
    
    implicit none

    integer, intent(in) ::aline, bline, dim
    real(kind=8), dimension(dim), intent(inout) :: vec
    real(kind=8) :: temp

    temp = vec(aline)
    vec(aline) = vec(bline)
    vec(bline) = temp

end subroutine vec_swap


!Takes a vector "vec" of dimension "dim" and finds the index in [from_index,dim]
!which takes the maximum absolute value
subroutine find_max(dim, vec, from_index, max_index)
    implicit none 

    integer , intent(in) :: dim, from_index
    real(kind=8), dimension(dim), intent(in) :: vec
    integer, intent(out) :: max_index

    integer :: temp

    max_index = from_index

    do temp=from_index,dim
        
        if (abs(vec(temp)) > abs(vec(max_index))) then
            max_index = temp
        end if
    
    end do

end subroutine find_max

!Given a n x n matrix A finds matrices L, U , P; two options : with and without partial pivoting
subroutine find_LU(A, n, L, U, P, partial_pivoting, b)
    
    implicit none

    real(kind=8), dimension(n,n), intent(in) :: A  !Initial matrix A
    real(kind=8), dimension(n,n), intent(out) :: L !Lower Triangular in the decomp
    real(kind=8), dimension(n,n), intent(out) :: P !Permutation Matrix 
    real(kind=8), dimension(n,n), intent(out) :: U !Upper Triangular in the decomp
    real(kind=8), dimension(n), intent(inout) :: b !initial vector b

    integer, intent(in) :: n !dimension of the matrix
    logical, intent(in) :: partial_pivoting

    integer :: i, j, max_pivot !helping variables




    if (partial_pivoting .eqv. .FALSE.) then

        U = A

        call ident(n,L) !initialize to identity

        call ident(n,P) !initialize to identity




        do i=1,n-1

            if (U(i,i)==0) then !in this case this algorothm will not work
                stop
            
            else

                do j = i+1, n

                    L(j,i) = U(j,i)/U(i,i)

                    U(j,:) = U(j,:) - L(j,i)*U(i,:)


                end do 

            end if

        end do

    else 

        U = A

        call ident(n,L) !initialize to identity

        call ident(n,P) !initialize to identity

        do i=1,n-1

            call find_max(n, U(:,i), i, max_pivot)

            call swap(n, U, i, max_pivot, i , n)

            call vec_swap(n, b, i, max_pivot)


            if (U(i,i) == 0) then
                cycle !if U(i,i) == 0 after the swaping then skip one iter
            end if

            call swap(n, L, i, max_pivot, 1 , i-1)

            call swap(n, P, i, max_pivot, 1 , n)


            do j = i+1, n

                L(j,i) = U(j,i)/U(i,i)

                U(j,i:n) = U(j,i:n) - L(j,i)*U(i,i:n)

            end do

        end do

    end if

end subroutine find_LU

end module LU_decomp