module read_data

implicit none

integer, save :: n  !Scope to the driver
real, allocatable, dimension(:), save :: b    !Scope to the driver  
real, allocatable, dimension(:,:), save :: A  !Scope to the driver

contains

!Counts the number of entries in a file  containing a colunm-vector  
  subroutine count_vec_entries(filename)
    implicit none
    
    character(len=:), allocatable, intent(in) :: filename

    integer :: IOstatus
    
    real(kind=8) :: tmp !dummy var

    !Adds number of real entries til EOF

    n = 0
    
    open(1, file = filename) !Open "filename" file and refer to it by number 1

    do
      
      read(1, *, iostat = IOstatus) tmp
      
      if (IOstatus==0) then

        n = n+1

      else if (IOstatus/=0) then
        exit

      end if 

    end do
    
    close(1)

  end subroutine count_vec_entries

  !!Reads in vector b of known size n
  subroutine read_vector(bname)
    implicit none
    character(len=:), allocatable, intent(in) :: bname

    !Filling the vector b
    open(12,file=bname)
    allocate(b(n))
    read(12,*) b
    close(12)

  end subroutine read_vector

  
  subroutine read_matrix(Afilename)
    
    implicit none
    character(len=:), allocatable, intent(in) :: Afilename

    
    !Filling the matrix A
    allocate(A(n,n))
    open(3,file=Afilename)
    read(3,*) A
    A = transpose(A)
    close(3)
  
  end subroutine read_matrix

end module read_data





