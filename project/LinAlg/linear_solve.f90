!Driver for LU solving
program linear_solve

  use forward_solve,    only : find_y
  use backward_solve,   only : find_x
  use write_data,       only : write_solution
  use write_to_screen,  only : print_inputs, print_matrix, print_vector
  use LU_decomp
  use read_data

  implicit none

  real, allocatable, dimension(:,:) :: L, U, P
  real, allocatable, dimension(:) :: x, y

  character(len=:), allocatable :: outfile, Afile, bfile
  Afile = "A_1.dat"
  bfile = "b_1.dat"
 

  
  !Read the read_data
  call count_vec_entries(bfile)   !this gives number "n"
  call read_matrix(Afile)       !this gives matrix "A"
  call read_vector(bfile)       !this gives vector "b"

  !Sanity check
  call print_inputs(A,b,n)

  !We have n ---> Allocate L, U, P, x, y
  allocate(L(n,n))
  allocate(U(n,n))
  allocate(P(n,n))
  allocate(x(n))
  allocate(y(n))

  print*,"LU without Pivoting"
  call find_LU(A, n, L, U, P, .False., b)
  outFile = "x_1.dat"
  print*,"------------------------------------"
  print*,"L = "
  call print_matrix(L,n)

  print*,"------------------------------------"
  print*,"U = "
  call print_matrix(U,n)

  call find_y(n, L, y, b)
  call find_x(n, U, x, y)

 
  call write_solution(outFile,x,n)
  print*,"------------------------------------"


  print*,
  print*,"LU with Partial Pivoting"
  call find_LU(A, n, L, U, P, .True., b)
  outFile = "p_1.dat"
  
  print*,"------------------------------------"
  print*,"L = "
  call print_matrix(L,n)

  print*,"------------------------------------"
  print*,"U = "
  call print_matrix(U,n)

  call find_y(n, L, y, b)
  call find_x(n, U, x, y)

  call write_solution(outFile,x,n)

  !Deallocate everything
  deallocate(L)
  deallocate(U)
  deallocate(P)
  deallocate(x)
  deallocate(y)

end program linear_solve