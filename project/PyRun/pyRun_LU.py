import os
import numpy as np
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt


#Runs the executable "linear_testing.exe"

def run_linear():
   os.system("./linear_testing.exe")


#checks if "afile" is already in the current dir and if it is, saves it with next version

def rename_dat_file(afile):
   if afile in os.listdir(os.curdir):
      file_name_parts = afile.split('.')
      first_parts = file_name_parts[0].split('_')
      
      count = 1
      while (first_parts[0] + '_' + str(count) + "." + file_name_parts[1]) in os.listdir(os.curdir):
         count = count+1
      os.system("mv "+ afile + " " + first_parts[0] + '_' + str(count) + "." + file_name_parts[1])




def make_make():

   #change working directory to Fortran files
   os.chdir("../LinAlg")
   
   #if no executable -> compile
   if "linear_testing.exe" not in os.listdir(os.curdir):
      os.system("make")

   #if we have an executable -> first clean and then compile
   else:
      os.system("make clean")
      os.system("make")
   



   """
   Creates runtime parameters init file so that .init is the active file
      and .init1 and .init2 etc are the previous files used
   Many default input values are selected with only the threshold required
   """


def setup_dat(A, b):

	#if we already have 1.dat files, rename them to the next available number
	rename_dat_file("A_1.dat")
	rename_dat_file("b_1.dat")

	#No 1.dat file exist anymore, create it...
   	f = open("A_1.dat","w")
   	g = open("b_1.dat","w")

   	for row in A:
   		for element in row:
   			f.write(str(element) + "\t")
   		f.write('\n') #change line
   	f.close()

   	for element in b:
   		g.write(str(element))
   		g.write('\n') #change line
   	g.close()


def read_vector(filename):

	#change dir to the working one where the .dat files are
	#os.chdir("../LinAlg/Testing_whole")
	x = []
	inp = open (filename ,"r")

	#read line into array 
	for line in inp.readlines():
	# loop over the elemets, split by whitespace
		for i in line.split():
		# convert to float and append to the list
			x.append(float(i))

	inp.close()

	return x

def error_check(vec1, vec2, threshold):
	Test = False
	
	x = np.asarray(vec1, dtype=np.float32)
	y = np.asarray(vec2, dtype=np.float32)
	
	if (np.sum(abs(x-y)) < threshold):
		Test = True
	return Test



def print_err_check(solnum):

	x_1 = read_vector("x_1.dat")
	p_1 = read_vector("p_1.dat")

	Check_x1 = error_check(x_1, solnum, threshold)
	Check_p1 = error_check(p_1, solnum, threshold)
	
	print " "
	print "Solution Check without partial pivoting (threshold value = ", threshold, ")"
   	print "-------------------------------------------------------------------------"
   
   	if (Check_x1 == True):
   		print "Pass"
   	else:
   		print "Fail"


   	print " "
   	print "Solution Check with partial pivoting (threshold value = ", threshold, ")"
   	print "----------------------------------------------------------------------"
   
   	if (Check_p1 == True):
   		print "Pass"
   	else:
   		print "Fail"

   	print " "


def matrix_plot(matrix):

   
   n = len(matrix)
   figA = plt.figure()
   plt.imshow(matrix, interpolation='nearest', cmap=plt.cm.ocean,
              extent=(0.5,n+0.5,0.5,n+0.5))
   plt.colorbar()
   plt.suptitle("Plot of matrix " + "A")
   plt.savefig("A_1.png",dpi=150)
   plt.show()

def vectors_plot(y, z):

	y = np.array(y)
	z = np.array(z) 

	n = len(y)
	
	ymin = min(min(y),min(z))-0.025
   	ymax = 1.025*max(max(y),max(z))

	x = np.linspace(0,len(y),num=len(y))

	bar = np.linspace(ymin, ymax, num=7)

	extent = (0.5,n+0.5,0.5,n+0.5)#[x[0]-(x[1]-x[0])/2., x[-1]+(x[1]-x[0])/2.,0,1]

	f, axarr = plt.subplots(2, sharex=True)
	cax = axarr[0].imshow(y[np.newaxis,:], cmap=plt.cm.ocean, aspect="auto", extent=extent)
	axarr[0].set_title('Solution x vector')
	axarr[0].set_xlim(extent[0], extent[1])
	axarr[0].set_yticks([])
	axarr[0].set_xticks([])
	

	axarr[1].imshow(z[np.newaxis,:], cmap=plt.cm.ocean, aspect="auto", extent=extent)
	axarr[1].set_title('Vector b')
	axarr[1].set_xlim(extent[0], extent[1])
	axarr[1].set_yticks([])
	axarr[1].set_xticks([])

	f.subplots_adjust(right=0.8)
	f.colorbar(cax, ax=axarr.ravel().tolist(), ticks=bar)
	plt.savefig("xb_1.png",dpi=150)
	plt.show()





if __name__ == "__main__":
   
   #run these functions only if we want to actually use them
   #ie, dont run make_make if we don't want to recompile or clean
   #    don't run runtimeParamters_init if we don't want to create a new file

   #use these particular input parameters
   

   #Testing matrices
   A_1 = [[1, 1, -1],[1, 2, -2],[-2, 1, 1]]
   A_2 = [[4, 3, 2, 1], [3, 4, 3, 2], [2, 3, 4, 3], [1, 2, 3, 4]]
   A_3 = [[1, -1, 1, -1], [-1, 3, -3, 3], [2, -4, 7, -7], [-3, 7, -10, 14]]

   All_A = [A_1, A_2, A_3]
   #Testing vectors
   b_1 = [1, 0, 1]
   b_2 = [1, 1, -1, -1]
   b_3 = [0, 2, -2, -8]

   All_b = [b_1, b_2, b_3]

   # Determine the solutions using numpy
   sol_1 = np.linalg.solve(A_1,b_1)
   sol_2 = np.linalg.solve(A_2,b_2)
   sol_3 = np.linalg.solve(A_3,b_3)

   All_sol = [sol_1, sol_2, sol_3]
   
   threshold = 1.e-14

   make_make()

   for i in range(3):
   	
   	setup_dat(All_A[i], All_b[i])
   	run_linear()

   	print_err_check(All_sol[i])
	
	

   	
   	#ploting
	matrix_plot(All_A[i])
   	vectors_plot(All_sol[i], All_b[i])
   	if (i!=2):
   		rename_dat_file("x_1.dat")
		rename_dat_file("p_1.dat")
   		rename_dat_file("A_1.png")
		rename_dat_file("xb_1.png")
