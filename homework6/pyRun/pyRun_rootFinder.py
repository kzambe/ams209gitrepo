"""
Directory structure:
   hw6/
   |-- newton_rootFinder/  RootFinder.F90
   |        findRootMethod_module.F90
   |        makefile
   |        read_initFile_module.F90
   |        definition.h
   |        ftn_module.F90
   |        output_module.F90
   |        setup_module.F90
   |        (excluding rootFinder.init)
   |
   |-- pyRun/     pyRun_rootFinder.py
"""

#import necessary Python modules

import os
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt


#Runs the executable "rootFinder.exe"

def run_rootFinder():
   os.system("./rootFinder.exe")


#checks if "afile" is already in the current dir and if it is, saves it with next version

def rename_file(afile):
   if afile in os.listdir(os.curdir):
      file_name_parts = afile.split('.')
      
      count = 1
      while (file_name_parts[0] + str(count) + "." + file_name_parts[1]) in os.listdir(os.curdir):
         count = count+1
      os.system("mv "+ afile + " " + file_name_parts[0] + str(count) + "." + file_name_parts[1])




def make_make():

   #change working directory to Fortran files
   os.chdir("../newton_rootFinder/")
   
   #if no executable -> compile
   if "rootFinder.exe" not in os.listdir(os.curdir):
      os.system("make")

   #if we have an executable -> first clean and then compile
   else:
      os.system("make clean")
      os.system("make")
   



   """
   Creates runtime parameters init file so that .init is the active file
      and .init1 and .init2 etc are the previous files used
   Many default input values are selected with only the threshold required
   """

def runtimeParameters_init(threshold, run_name="newton", method_type="newton",
   x_beg=-10.0, x_end=10.0, max_iter=1000, ftn_type=2, init_guess=1.5, multiplicity=2):
   
   #if we already have an init file, rename it to the next available number
   rename_file("rootFinder.init")

   #No rootFinder.init file exist anymore, create it...
   f = open("rootFinder.init","w")
   
   #...and now write it

   #list of all parameters in .init file
   parameters = ["run_name","method_type","x_beg","x_end","max_iter","threshold","ftn_type","init_guess","multiplicity"]
   
   for parameter in parameters:
      #write it so run_name and method_type are clear they are strings
      if parameter in ["run_name","method_type"]:
         f.write(parameter+ "\t\t\t" + "\'" + str(eval(parameter)) + "\'")
      else:
         f.write(parameter+ "\t\t\t" +str(eval(parameter)))
      f.write('\n') #change line
   f.close()


#ploting data

def plot_data(plotafile, threshold, rename_dat=True):
   
   #import dat file into data array
   data = np.loadtxt("rootFinder_newton.dat")
   
   #dat files structure:
   # iteration number, search result x, function value f(x), and residual

   
   
   ymin = 1.025*min(data[:,1])
   ymax = 1.025*max(data[:,1])

   
   #produce figure iter vs solutions
   plt.figure(figsize=(12,8),dpi=150)
   plt.plot(data[:,0],data[:,1],'o-',color='blue')
   plt.xlabel("Number of Iterations")
   plt.ylabel("x")
   #plt.title("Iterations vs Search Result")
   plt.ylim(ymin,ymax)


   #plt.suptitle('Threshold value: '+str(threshold))
   plt.savefig(plotafile, dpi=150)

   plt.show()

   #if we already have a dat file -> rename it to the next available number
   if rename_dat:
      rename_file("rootFinder_newton.dat")




def plot_data_zoomed(plotafile, threshold, rename_dat=True):
   
   '''
   This method prodeuce figures but keeps only the points from iter 30 and beyond
   '''

   #import dat file into data array
   data = np.loadtxt("rootFinder_newton.dat")
   
   
   #dat files structure:
   # iteration number, search result x, function value f(x), and residual

   ymin = 0.8*min(data[34:,1])
   ymax = 1.2*max(data[34:,1])

   
   #produce figure iter vs solutions
   plt.figure(figsize=(12,8),dpi=150)
   plt.plot(data[34:,0],data[34:,1],'o-',color='blue')
   plt.xlabel("Number of Iterations")
   plt.ylabel("x")
   #plt.title("Iterations vs Search Result [Zoomed]")
   plt.ylim(ymin,ymax)


   #plt.suptitle('Threshold value: '+str(threshold))
   plt.savefig(plotafile, dpi=150)

   plt.show()

   #if we already have a dat file -> rename it to the next available number

   if rename_dat:
      rename_file("rootFinder_newton.dat")

if __name__ == "__main__":
   
   #run these functions only if we want to actually use them
   #ie, dont run make_make if we don't want to recompile or clean
   #    don't run runtimeParamters_init if we don't want to create a new file

   #use these particular input parameters
   
   my_ftn_type = 3

   thresholds = ["1.e-4","1.e-6","1.e-8"]
   code_thres = ["4", "6",  "8"]

   #set runtime parameters, call functions properly to execute fortran code
   #  used for 3 different thresholds on same function for comparison
   
   make_make()

   for i in range(len(thresholds)):
      #obtain results for  initial guess
      runtimeParameters_init(thresholds[i], ftn_type = my_ftn_type, init_guess = 3)
      run_rootFinder()

      plotName = "graph_near_" + str(my_ftn_type) + "_" +str(code_thres[i])+ ".png"
      plot_data(plotName, thresholds[i], rename_dat = not (i==len(thresholds)))  #don't rerename_dat dat file names if it's the last element

      #obtain results for close initial guess
      
      runtimeParameters_init(thresholds[i],ftn_type= my_ftn_type,init_guess = -10.2,x_beg=-1000000.0, x_end=100000.0)
      run_rootFinder()
      
      plotName = "graph_far_" + str(my_ftn_type) + "_" + str(code_thres[i]) + ".png"
      plot_data(plotName, thresholds[i], rename_dat = False )  #don't rerename_dat dat we will make also the zoomed version
      
      plotName = "graph_zoomed_far_" + str(my_ftn_type) + "_" + str(code_thres[i]) + ".png"
      plot_data_zoomed(plotName, thresholds[i], rename_dat = not (i==len(thresholds)))  #don't rerename_dat dat file names if it's the last element



